package org.flyveleder.model;

import org.flyveleder.exceptions.FlyvelederModelException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

public class TrafficController extends User{

    public final static int TRAFFICCONTROLLER = 4;

    private static int maxId = 0;                  //static variable for the creation of ids

   // private int typeOfUser;
    private String id;                         //Unique id of the user
  //  private String name;                       // Login name
    private String BHVLicense;
    private Timesheet timeSheet;                  //reference to the Timesheet ledger
    private User boss;
    private ArrayList<LocalDate> vacationDates;              //The vacation dates of a controller



    public TrafficController(String name, int typeOfUser, User trafficController, Timesheet timeSheet) {
        super( name,typeOfUser,trafficController,timeSheet);
        vacationDates = new ArrayList<>();
        this.id = Integer.valueOf(++maxId).toString();
            assert boss != null : "should be 2";
            assert timeSheet != null : "sheet name is null";
            assert !name.equals("") : "Empty Name";
            this.timeSheet = timeSheet;

        }
       // public int getTypeOfUser() {
//        return typeOfUser;
//    }

    public User getBoss() {
        return boss;
    }

    public String getBHVLicense() {
        return BHVLicense;
    }

    public boolean isTrafficControler() {
        return true;
    }

    public void setBHVLicense(String BHVLicense) {
        this.BHVLicense = BHVLicense;
    }

    public boolean isBHVer() {
        return BHVLicense == null;
    }

    //todo : This is dangerous. Refactor the code so that the defensive programming guidlines are adhered to
    public ArrayList<LocalDate> getVacationDates()
    {
        return new ArrayList<>(vacationDates);
    }

    public void addVacation(LocalDate date){
        vacationDates.add(date);

    }
    public void removeVacation(LocalDate date){
        vacationDates.remove(date);
    }
    /**
     * @return Projects the user has been planned in, in the future
     * @throws FlyvelederModelException
     */
    public ArrayList<Project> getProjects() throws FlyvelederModelException {
        ArrayList<Project> projects = new ArrayList<>();
        HashMap<String, TimesheetItem> timesheet = timeSheet.getTimeSheetForUser(id);
        ArrayList<TimesheetItem> items = new ArrayList(timesheet.values());
        //sort work on the date
        items.sort((w1, w2) -> w1.getDate().isBefore(w2.getDate()) ? -1 : 1);
        items.forEach(item -> {
            if (!projects.contains(item.getProject())) projects.add(item.getProject());
        });
        return projects;
    }


    /**
     * @return A list of all timesheet items for the user.
     * @throws FlyvelederModelException
     */
    public ArrayList<TimesheetItem> getWorkList() throws FlyvelederModelException {
        HashMap<String, TimesheetItem> timesheet = timeSheet.getTimeSheetForUser(id);
        ArrayList<TimesheetItem> items = new ArrayList(timesheet.values());
        return items;
    }

}
