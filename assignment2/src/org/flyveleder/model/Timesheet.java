package org.flyveleder.model;

import org.flyveleder.exceptions.FlyvelederModelException;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.stream.Collectors;

public class Timesheet {
    private final HashMap<String, TimesheetItem> timeSheet;

    public Timesheet() {
        timeSheet = new HashMap<>();
    }


    /**
     * Add an timesheet item to the ledger
     *
     * @param item
     * @throws FlyvelederModelException pre-condition: The item should not cause a double entry for the trafficcontroller. e.g.
     *                                  if the trafficcontroller is working on 2020-10-1 from 12:00 to 18:00 an attempt to add
     *                                  an item for this trafficcontroler for 2020-10-1 from 17:00 to 21:00 should fail with a
     *                                  FlyvelederModelException!
     *                                  <p>
     *                                  post-condition: the new item should be appendend in the ledger.
     */
    public void addItem(TimesheetItem item) throws FlyvelederModelException {
        //todo: make sure business rules are correctly implemented
        //todo: 404


        if (!timeConflicts(item)) {
            throw new FlyvelederModelException("You cannot plan items before time");
        }
//
//        if (item.getStart().isBefore(item.getEnd().plusHours(12))) {
//            throw new FlyvelederModelException("You cannot plan items before time");
//        }
        //24 june
        timeSheet.put(item.getId(), item);
    }

    //
    public void removeItem(TimesheetItem item) throws FlyvelederModelException {
        timeSheet.remove(item.getId());
    }

    public void removeItem(String itemid) throws FlyvelederModelException {
        timeSheet.remove(itemid);
    }


    public TimesheetItem getTimeSheetItem(String id) {
        return timeSheet.get(id);
    }

    /**
     * @return all items in the time sheet.
     * todo: This method is not save...
     * Explain why this is the case and change it so is is save
     */
    public HashMap<String, TimesheetItem> getItems() {
        return new HashMap<>(timeSheet);
    }


    /**
     * @param userid
     * @return all the timesheet items for a specific user
     * @throws FlyvelederModelException
     */
    public HashMap<String, TimesheetItem> getTimeSheetForUser(String userid) throws FlyvelederModelException {
        return (HashMap<String, TimesheetItem>) timeSheet.entrySet().stream()
                .filter(u -> u.getValue().getTrafficController().getId().equals(userid))
                .collect(Collectors.toMap(u -> u.getKey(), u -> u.getValue()));
    }

    /**
     * @param projectid
     * @return all the timesheet items for a specific project
     * @throws FlyvelederModelException
     */
    public HashMap<String, TimesheetItem> getTimeSheetForProject(String projectid) throws FlyvelederModelException {
        return (HashMap<String, TimesheetItem>) timeSheet.entrySet().stream()
                .filter(u -> u.getValue().getProject().getId().equals(projectid))
                .collect(Collectors.toMap(u -> u.getKey(), u -> u.getValue()));
    }


    private boolean timeConflict(TimesheetItem item) {


        for (TimesheetItem timesheetItem : timeSheet.values()) {
            System.out.println("Example: " + timesheetItem.getDate() + " " + timesheetItem.getStart() + " " + timesheetItem.getEnd());
            System.out.println("Target: " + item.getDate() + " " + item.getStart() + " " + item.getEnd());
        }


        return true;
    }


    private boolean timeConflicts(TimesheetItem item) {
        LocalDate date = item.getDate();
        LocalTime startTime = item.getStart();
        LocalTime endTime = item.getEnd();
        User trafficController = item.getTrafficController();

        for (TimesheetItem timesheetItem : timeSheet.values()) {
            System.out.println("Example: " + timesheetItem.getDate() + " " + timesheetItem.getStart() + " " + timesheetItem.getEnd());
            System.out.println("Target: " + item.getDate() + " " + item.getStart() + " " + item.getEnd());
            LocalDate currentDate = timesheetItem.getDate();
            LocalTime currentStartTime = timesheetItem.getStart();
            LocalTime currentEndTime = timesheetItem.getEnd();
            User trafficControllerSchedule = timesheetItem.getTrafficController();
            if (date.isEqual(currentDate) && (trafficController == trafficControllerSchedule)) {
                if (startTime.equals(currentStartTime) || (!startTime.equals(currentEndTime))) {
                    return false;
                }
                if (startTime.isBefore(currentEndTime) && startTime.isAfter(currentStartTime)) {
                    return false;
                }
                if (endTime.equals(currentStartTime) || endTime.equals(currentEndTime)) {
                    return false;
                }
                if (endTime.isAfter(currentStartTime) && endTime.isBefore(currentEndTime)) {
                    return false;
                }
                if (startTime.isBefore(currentStartTime) && endTime.isAfter(currentEndTime)) {
                    return false;
                }
            }
        }
        return true;
    }
}


//
//    private final HashMap<String, TimesheetItem> timeSheet;
//
//    public Timesheet() {
//        timeSheet = new HashMap<>();
//
//    }
//
//
//    /**
//     * Add an timesheet item to the ledger
//     *
//     * @param item
//     * @throws FlyvelederModelException pre-condition: The item should not cause a double entry for the trafficcontroller. e.g.
//     * if the trafficcontroller is working on 2020-10-1 from 12:00 to 18:00 an attempt to add
//     *  an item for this trafficcontroler for 2020-10-1 from 17:00 to 21:00 should fail with a
//     *  FlyvelederModelException!
//     *  <p>
//     *  post-condition: the new item should be appendend in the ledger.
//     */
//    public void addItem(TimesheetItem item) throws FlyvelederModelException {
//        //todo: make sure business rules are correctly implemented
//        //todo: 404
//        //24 june
//        if(!isWithoutTimeConflicts(item))throw new FlyvelederModelException("You cannot plan items before time");{
//
//        }
////
////        if (item.getStart().isBefore(item.getEnd().plusHours(12))) {
////            throw new FlyvelederModelException("You cannot plan items before time");
////        }
//        //24 june
//        timeSheet.put(item.getId(), item);
//    }
//
//    //
//    public void removeItem(TimesheetItem item) throws FlyvelederModelException {
//        timeSheet.remove(item.getId());
//    }
//
//    public void removeItem(String itemid) throws FlyvelederModelException {
//        timeSheet.remove(itemid);
//    }
//
//
//    public TimesheetItem getTimeSheetItem(String id) {
//        return timeSheet.get(id);
//    }
//
//    /**
//     * @return all items in the time sheet.
//     * todo: This method is not save...
//     * Explain why this is the case and change it so is is save
//     */
//    public HashMap<String, TimesheetItem> getItems() {
//        return new HashMap<>(timeSheet);
//    }
//
//
//    /**
//     * @param userid
//     * @return all the timesheet items for a specific user
//     * @throws FlyvelederModelException
//     */
//    public HashMap<String, TimesheetItem> getTimeSheetForUser(String userid) throws FlyvelederModelException {
//        return (HashMap<String, TimesheetItem>) timeSheet.entrySet().stream()
//                .filter(u -> u.getValue().getTrafficController().getId().equals(userid))
//                .collect(Collectors.toMap(u -> u.getKey(), u -> u.getValue()));
//    }
//
//    /**
//     * @param projectid
//     * @return all the timesheet items for a specific project
//     * @throws FlyvelederModelException
//     */
//    public HashMap<String, TimesheetItem> getTimeSheetForProject(String projectid) throws FlyvelederModelException {
//        return (HashMap<String, TimesheetItem>) timeSheet.entrySet().stream()
//                .filter(u -> u.getValue().getProject().getId().equals(projectid))
//                .collect(Collectors.toMap(u -> u.getKey(), u -> u.getValue()));
//    }
//    private boolean isWithoutTimeConflicts(TimesheetItem item){
//        LocalDate desirableDate=item.getDate();
//        LocalTime desirableStartTime=item.getStart();
//        LocalTime desirablEnd=item.getEnd();
//        User trafficcontroller=item.getTrafficController();
//
//        for(TimesheetItem timesheetItem:timeSheet.values()){
//            LocalDate currentDate=timesheetItem.getDate();
//            LocalTime currentStartTime=timesheetItem.getStart();
//            LocalTime currentEndItem=timesheetItem.getEnd();
//            User trafficControllerSchedule=timesheetItem.getTrafficController();
//            if(desirableDate.isEqual(currentDate)&& (trafficcontroller==trafficControllerSchedule)){
//                if (!desirableStartTime.equals(currentStartTime) || (!desirableStartTime.equals(currentEndItem))) {
//                    return true;
//                }
//            }
//    }
//        return false;
//    }
//}
