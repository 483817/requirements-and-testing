package org.flyveleder.model;

import org.flyveleder.exceptions.FlyvelederModelException;
import org.flyveleder.support.ProjectMailer;
import org.junit.jupiter.api.TestInstance;

import java.io.*;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by abe23 on 08/11/17.
 */

public class Project implements Serializable{

    private final String id;
    private final String projectName;
    private final User customer;
    private final Timesheet timeSheet;          //reference to the timesheet ledger
    private String Adress;
    private String Notes;
    private static int maxId = 0;
    private  ISendMail sendMail;
    private ProjectMailer mailer;


    public Project(String projectName, User customer, Timesheet timeSheet) {
        // First the "pre-condities" (i.e. what we expect to
        // be true when the program works correctly)
        // which say something about the parameters.
        assert projectName != null : " cant not be null project name ";                    // M
        assert !projectName.equals(" ") : " project can not  have empty anme ";                    // M
        assert customer != null : " cant not be null project name ";
        assert customer.getTypeOfUser() == 1 : " customer is 1";
        assert timeSheet != null : "time sheer is required";
        this.id = "" + ++maxId;
        this.projectName = projectName;
        this.customer = customer;
        this.timeSheet = timeSheet;

    }


    /**
     * @return the total time spend on a project
     * @throws FlyvelederModelException
     */
    public long getTotalTime() throws FlyvelederModelException {
        Duration totaltime = Duration.ofHours(0);
        HashMap<String, TimesheetItem> timesheet = timeSheet.getTimeSheetForProject(id);
        totaltime = timesheet.values().stream().map(unit ->
                Duration.between(unit.getStart(), unit.getEnd())
        )
                .reduce(totaltime, (sum, time) -> sum.plus(time));
        return totaltime.getSeconds();
    }

    public String getId() {
        return id;
    }

    public String getProjectName() {
        return projectName;
    }

    public User getCustomer() {
        return customer;
    }

    public String getAdress() {
        return Adress;
    }

    public String getNotes() {
        return Notes;
    }


    public void setAdress(String adress) {
        Adress = adress;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }


    private String getMailTemplatee() {
       if (!sendMail.isSent(" ")){
           throw new RuntimeException("nt sent");}
        return "not sent";
    }

//private String getMailTemplate() {
//    InputStream is = null;
//    String template = "";
//    try {
//        is = new FileInputStream("./database/mailtemplate.tpl");
//        BufferedReader buf = new BufferedReader(new InputStreamReader(is));
//        String line = buf.readLine();
//        StringBuilder sb = new StringBuilder();
//        while (line != null) {
//            sb.append(line).append("\n");
//            line = buf.readLine();
//        }
//        template = sb.toString();
//    } catch (FileNotFoundException e) {
//        e.printStackTrace();
//    } catch (IOException e) {
//        e.printStackTrace();
//    }
//    return template;
//}

    public void sendMail() {
        ProjectMailer mailer = new ProjectMailer(this,sendMail);
        mailer.setTemplate(getMailTemplatee());
        mailer.sendMail();

    }

    /**
     * @return all the timesheet items belonging to this project.
     * @throws FlyvelederModelException
     */
    public ArrayList<TimesheetItem> getTimeSheet() throws FlyvelederModelException {
        return new ArrayList(timeSheet.getTimeSheetForProject(id).values());
    }



}