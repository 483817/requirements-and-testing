package org.flyveleder.model;


import java.util.ArrayList;


public class Manager extends User {

    public final static int MANAGER = 3;
    //private int typeOfUser;
    public ArrayList<User> employees;


    public Manager(String name, User boss, Timesheet timeSheet) {
        super(name,3,boss,timeSheet);
        //assert name != null : "Name cannot be null";
        employees = new ArrayList<>();
        if (boss != null) {
            boss.getEmployees().add(this);
        }

    }

//    public int getTypeOfUser() {
//        return typeOfUser;
//    }
    public ArrayList<User> getEmployees() {
        return employees;
    }
    public boolean isManager() {
        return true;
    }


}
