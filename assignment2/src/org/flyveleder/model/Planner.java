package org.flyveleder.model;

import org.flyveleder.exceptions.FlyvelederModelException;
import org.mindrot.jbcrypt.BCrypt;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

public class Planner extends User{
    public final static int PLANNER = 2;
    private static int maxId = 0;                  //static variable for the creation of ids
   // private final int typeOfUser;

    public Planner(String name, int typeOfUser, User planner, Timesheet timeSheet) {
        super(name,typeOfUser,planner,timeSheet);
       // assert name != null : "Name cannot be null";
        assert 1 <= typeOfUser : "smaller than 4";
        assert typeOfUser <= 4 : "bigger than 4";
      //  this.typeOfUser = typeOfUser;

    }

//    public int getTypeOfUser() {
//        return typeOfUser;
//    }

    public boolean isPlanner() {
        return  true;
    }



}
