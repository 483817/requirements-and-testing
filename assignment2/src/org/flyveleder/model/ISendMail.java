package org.flyveleder.model;

public interface ISendMail {
    public boolean isSent(String mail);

}
