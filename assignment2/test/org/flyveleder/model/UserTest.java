package org.flyveleder.model;

import org.flyveleder.exceptions.FlyvelederModelException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserTest {

    private User boss;
    private Timesheet timesheet;
    private Project project;
    private User customer;
    private User planner;
    private ArrayList<LocalDate>vacationDates;



    @BeforeAll
    public void startUp() {
        boss = new User("bossman", User.MANAGER, null, null);
        customer = new User("customer", User.CUSTOMER, null, null);
        planner = new User("planner", User.PLANNER, boss, null);
        timesheet = new Timesheet();
        project = new Project("Test", customer, timesheet);

    }


    /**
     * good weather test cases for the User class
     * @link User#User(java.lang.String, int,userObject,timesheet, )},
     *      {@link User#getName()},
     *      {@link User#getTypeOfUser()},
     *      {@link User#getBoss()} ()},
     *      {@link User#toString()}.
     *
     */

    @Test
    public void testUser() throws FlyvelederModelException {
        /** testing the constructor    **/


        User user = new User("a name", User.TRAFFICCONTROLLER, boss, timesheet);
        //checking certain values of methods.
        assertNotNull(user, "User should not be null");
        assertEquals(user.getTypeOfUser(), User.TRAFFICCONTROLLER);
        assertEquals(user.getName(), "a name");
        assertSame(user.getBoss(), boss);
        assertNotSame(user.getBoss(), User.CUSTOMER);
        assertNotSame(user.getBoss(), null);
        assertEquals(user.getId(), "4"); // second user created... id should be 2
        assertNotEquals(user.getId(), "2"); // second user created...
        assertEquals("trafficcontroller,4,a name", user.toString());

        user.setBHVLicense("bhv license");
        assertEquals("bhv license",user.getBHVLicense(),"user is BHVer");
        assertFalse(user.isBHVer(),"user is not Bhver");



        /* Testing getters and setters to get 100% code coverage */
        /*TODO: is it userfull to test all the getters and setters ? */
        assertEquals("bossman", boss.getName());
        assertFalse(boss.isTrafficControler());
        assertTrue(boss.isManager());


        //testing type of users
        assertEquals(customer.getTypeOfUser(), User.CUSTOMER);
        assertEquals(boss.getTypeOfUser(), User.MANAGER);
        assertEquals(planner.getTypeOfUser(), User.PLANNER);

        assertEquals(customer.getUserType(), "customer");
        assertEquals(boss.getUserType(), "manager");
        assertNotEquals(boss.getTypeOfUser(), "planner");
        assertEquals(planner.getUserType(), "planner");
        assertNotEquals(planner.getTypeOfUser(), "customer");

        TimesheetItem item = new TimesheetItem(user, project, LocalTime.of(12, 0), LocalTime.of(14, 0), LocalDate.now());

        assertEquals(user.getWorkList().size(), 0);

//
        assertFalse(user.isCustomer());
        assertFalse(user.isManager());
        assertFalse(user.isPlanner());
        assertTrue(boss.isManager());
        assertTrue(customer.isCustomer());
//checking method vacationDates
        assertNotNull(user.getVacationDates());
        LocalDate today = LocalDate.now();
        user.addVacation(today);
        assertSame(user.getVacationDates().get(0), today);


        LocalDate today1 = LocalDate.now();
        user.addVacation(today1);
        assertSame(user.getVacationDates().get(1), today1);
        assertSame(user.getVacationDates().size(), 2);
        user.removeVacation(today1);
        assertSame(user.getVacationDates().size(), 1);

        assertNotNull(boss.getEmployees());
        assertEquals(boss.getEmployees().size(), 2);

        //test password management

        user.changePassword("hello");
        assertTrue(user.login("hello"));
        assertFalse(user.login("helloworld"));

        //testing getters and setters
        // test name....set name and get name by using assert equals
        user.setName("Mooza");
        assertEquals("Mooza",user.getName());
        // test userName....set username and get username by using assert equals
        user.setUserName("Mooza");
        assertEquals("Mooza",user.getName());

    }


    /**
     * Bad weather tests for the User class
     *  Some bad weather test cases:
     *  Each test will be aborted with an
     *  Assertion exception because we try to do
     *  something forbidden.
     */
    @Test
    public void testBadUser() {
        //todo: Creating a user with an unknown role should fail
        // a number which is higher than 4 or smaller than 1
        //lower boundary 1
        //upper boundary 4
        assertThrows(AssertionError.class, () -> {
                    User user = new User("a name", 12, boss, timesheet);
                },
                "Creating a user with an unknown role should fail");

        //todo: Creating a trafficcontroller without a boss should fail, create the test!
        assertThrows(AssertionError.class, () -> {
            User user = new User("a name", 4, null, timesheet);
        }, "Creating a traffic controller without a boss should fail");


        //todo: Creating a trafficcontroller without a timesheet should fail
        assertThrows(AssertionError.class, () -> {
            User user = new User("a name", 4, boss, null);
        }, "Creating a traffic controller without a boss should fail");


        //todo: Creating a trafficcontroller without a name should fail
        assertThrows(AssertionError.class, () -> {
            User user = new User("", 4, boss, timesheet);
        }, "Creating a traffic controller without a boss should fail");


        //todo: Creating a trafficcontroller with null name should fail
        assertThrows(AssertionError.class, () -> {
            User user = new User(null, 4, boss, timesheet);
        }, "Creating a traffic controller without a boss should fail");


    }


    // ==========================================
    // Code to check the 'asserts-enabled' status
    // (Otherwise all the bad-weather tests will fail even if assertions have been added)
    static {
        boolean ea = false;
        assert ea = true; // mis-using a side-effect !
        if (!ea)
            System.err.println("** WARNING: ASSERTS ARE NOT ENABLED **");
    }


}