package org.flyveleder.model;

import org.flyveleder.exceptions.FlyvelederModelException;
import org.flyveleder.support.ProjectMailer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ProjectTest {
    private Timesheet timesheet;
    private User Customer;
    private User boss;
    private Project project;
    private User customer;
    private User planner;
    private ProjectMailer mailer;


    @BeforeAll
    public void startUps() {
        timesheet = new Timesheet();
        planner = new User("planner", User.PLANNER, boss, null);
        customer = new User("customer", User.CUSTOMER, null, timesheet);
        project = new Project("project1", customer, timesheet);

    }

    @Test
    public void testProject() throws FlyvelederModelException {
        project = new Project("project1", customer, timesheet);
        assertEquals(project.getProjectName(), "project1");
        assertNotSame(project.getProjectName(), "my project", "project name issue");
        assertSame(project.getCustomer(), customer);
        assertNotSame(project.getTimeSheet(), timesheet);

//        project.setNotes(" this is note for project");
//        assertEquals(project.getNotes(), "this is note for project");


    }
    /**
     * Bad weather tests for the Project class
     *  Some bad weather test cases:
     *  Each test will be aborted with an
     *  Assertion exception because we try to do
     *  something forbidden.
     */
    //Creating project with null name
    @Test
    public void testProjectNullName() {
        assertThrows(AssertionError.class, () -> {
            new Project(null, Customer, timesheet); // null kind not allowed
            /* NOTREACHED */
        }, "null project not allowed");
    }
    //Creating project with empty name
    @Test
    public void testProjectNotEmpty() {
        assertThrows(AssertionError.class, () -> {
            new Project(" ", Customer, timesheet); // empty kind not allowed
            /* NOTREACHED */
        }, "cant not be empty project not allowed");
    }

    //Creating project without customer
    @Test
    public void testProjectWithoutCustomer() {
        assertThrows(AssertionError.class, () -> {
            new Project("project1", null, timesheet); // null customer not allowed
            /* NOTREACHED */
        }, "project cant not be created without customer ");
    }
    //Creating project without customer
    @Test
    public void testProjectWithoutTimesheet() {
        assertThrows(AssertionError.class, () -> {
            new Project("project1", Customer, null); // null timesheet not allowed
            /* NOTREACHED */
        }, "project cant not be created without timesheet ");
    }


    @Test
    public void testingEmailSending() {
        ISendMail sentMail = new ISendMail() {
            @Override
            public boolean isSent(String mail) {
                return true;
            }
        };
        ProjectMailer mailer = new ProjectMailer(project, sentMail);


        ISendMail notSentMail = new ISendMail() {
            @Override
            public boolean isSent(String mail) {
                return false;
            }
        };
        assertThrows(RuntimeException.class, () -> {
            ProjectMailer mailer1 = new ProjectMailer(project, notSentMail);
        }) ;
       }
    }

