package org.flyveleder.model;

import org.flyveleder.exceptions.FlyvelederModelException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TimesheetTest {
    private TimesheetItem timesheetItem;
    private Timesheet timesheet;
    private User planner;
    private User boss;
    private Project project;
    private String id;
    private User trafficController;
    private User customer;
    private LocalTime start, end;
    private LocalDate date;
    private boolean validated;

    //24 june
    @BeforeAll
    public void startUps() throws FlyvelederModelException {

        timesheet = new Timesheet();
        planner = new User("planner", User.PLANNER, boss, null);
        customer = new User("customer", User.CUSTOMER, null, timesheet);
        trafficController = new User("trafficController", 2, boss, timesheet);
        project = new Project("project", customer, timesheet);
        timesheetItem = new TimesheetItem(trafficController, project, start, end, date);
    }

    /**
     * Bad weather tests for the TimeSheet class
     */
//    @Test
//    public void TimeSheetBadWeatherTest() throws FlyvelederModelException {
//        timesheet.addItem(timesheetItem);
//        assertNotEquals(timesheetItem, timesheet.getTimeSheetItem(Integer.valueOf(-1).toString()));
//
//        assertThrows(FlyvelederModelException.class, () -> {
//                    TimesheetItem timesheetItemTest = new TimesheetItem(trafficController, project, start, end.plusHours(1), date);
//                    timesheet.addItem(timesheetItemTest);
//
//                },
//                "Timesheet with time clash should not be added");
//
//        assertThrows(FlyvelederModelException.class, () -> {
//                    TimesheetItem timesheetItemTest = new TimesheetItem(trafficController, project, start.minusHours(1), end, date);
//                    timesheet.addItem(timesheetItemTest);
//
//                },
//                "Timesheet with time clash should not be added");
//        assertThrows(FlyvelederModelException.class, () -> {
//                    TimesheetItem timesheetItemTest = new TimesheetItem(trafficController, project, start.plusHours(1), end.plusHours(1), date);
//                    timesheet.addItem(timesheetItemTest);
//
//                },
//                "A traffic controller schedules twice in the same period of the time  should fail");
//        assertThrows(FlyvelederModelException.class, () -> {
//                    TimesheetItem timesheetItemTest = new TimesheetItem(trafficController, project, end, end.plusHours(1), date);
//                    timesheet.addItem(timesheetItemTest);
//
//                },
//                "A traffic controller should not be scheduled twice in the same time");
//
//        assertThrows(FlyvelederModelException.class, () -> {
//                    TimesheetItem timesheetItemTest = new TimesheetItem(trafficController, project, start.minusHours(1), end.minusHours(1), date);
//                    timesheet.addItem(timesheetItemTest);
//
//                },
//                "A traffic controller should not be scheduled twice in the same time");
//
//        assertThrows(FlyvelederModelException.class, () -> {
//                    TimesheetItem timesheetItemTest = new TimesheetItem(trafficController, project, start.minusHours(1), end.plusHours(1), date);
//                    timesheet.addItem(timesheetItemTest);
//
//                },
//                "A traffic controller should not be scheduled twice in the same time");
//
//
//    }
//
    @Test
    public void TimeSheetMoreBadWeatherTest() throws Exception {

        //  todo:Creating a timeSheetItem  without a trafficController should fail
        assertThrows(AssertionError.class, () -> {
                    TimesheetItem item = new TimesheetItem(null, project, start, end, date);
                },
                "timesheet without traffic controllers should fail");

        // todo:Creating a timeSheetItem  without a project should fail
        assertThrows(AssertionError.class, () -> {
                    TimesheetItem item = new TimesheetItem(trafficController, null, start, end, date);
                },
                " T.S only possible for a project,it cant be created");
    }

    static {
        boolean ea = false;
        assert ea = true; // mis-using a side-effect !
        if (!ea)
            System.err.println("** WARNING: ASSERTS ARE NOT ENABLED **");
    }


    @Test
    public void TimeSheetGoodWeatherTestt() throws Exception {
        start = LocalTime.now();
        end = start.plusHours(5);
        date = LocalDate.now();
        TimesheetItem ourTimeSheetItem = new TimesheetItem(trafficController, project, start, end, date);
        start = end.plusHours(12);
        end = start.plusHours(4);
        date = date.plusDays(2);
        TimesheetItem ourSecondTimeSheetItem = new TimesheetItem(trafficController, project, start, end, date);

        timesheet.addItem(ourTimeSheetItem);
        timesheet.addItem(ourSecondTimeSheetItem);
        assertEquals(ourTimeSheetItem, timesheet.getTimeSheetItem(ourTimeSheetItem.getId()));
        assertEquals(ourSecondTimeSheetItem, timesheet.getTimeSheetItem(ourSecondTimeSheetItem.getId()));

    }
}
