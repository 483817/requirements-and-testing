package org.flyveleder.model;

import org.flyveleder.exceptions.FlyvelederModelException;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

class TimsheetItemTest {
    private User trafficController;
    private Project project;
    private LocalTime start, end;
    private LocalDate date;
    //getProject()
    //getTrafficController()
    //getUser
    //getDate
    //getEnd


    @Test
    public void testTimesheetItem() throws FlyvelederModelException {

        TimesheetItem timesheetItem = new TimesheetItem(trafficController, project, LocalTime.now(), LocalTime.now().plusHours(8), LocalDate.of(2020, Month.JUNE, 20));
        assertEquals(project, timesheetItem.getProject());
        assertNotSame("project1", timesheetItem.getProject(), "project name not correct");
        assertNotSame("planner", timesheetItem.getTrafficController(), "ts only for T.C");
        assertEquals(trafficController, timesheetItem.getTrafficController());
        assertNotSame("planner", timesheetItem.getTrafficController());
        assertNotSame(LocalTime.now().plusHours(8), timesheetItem.getStart(), "start date not matched");
        assertNotSame(LocalTime.now(), timesheetItem.getEnd(), "time not matched ...");


    }
    //
    //bad weather
    //assertEquals(LocalDate.now(), timesheetItem.getDate(),"not good its in  past");
    // assertEquals(LocalTime.now(), timesheetItem.getEnd(), "time not matched...");
    // assertEquals(LocalTime.now().plusHours(5),timesheetItem.getStart(),"end is not before satrt");


    /**
     * Bad weather tests for the TimeSheet class
     */
    @Test
    public void TimeSheetBadWeatherTest() {
        //todo: Creating a timesheet Item with an unknown user should fail


        //todo: Creating a timeSheetItem  without a trafficController should fail
        assertThrows(NullPointerException.class, () -> {
                    TimesheetItem item = new TimesheetItem(null, project, start, end, date);
                },
                "timesheet without traffic controllers should fail");

       //todo: Creating a timeSheetItem without a name should fail

       //todo: Creating a timeSheetItem with null name should fail

    }


}
